from django.db import models
from django.urls import reverse


class AutomobilesVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    def __str__(self):
        return f"{self.vin}"


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField()
    def __str__(self):
        return f"{self.name}"


class Appointment(models.Model):
    customer_name = models.CharField(max_length=100)
    vin = models.CharField(max_length=17)
    reason = models.CharField(max_length=250)
    datetime = models.DateTimeField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE,
    )
    is_vip = models.BooleanField(default=False)
    is_completed = models.BooleanField(default=False)
    def __str__(self):
        return f"{self.vin}"
    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.id})
