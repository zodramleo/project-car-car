import React from 'react';
import { NavLink } from 'react-router-dom';


function VehicleModelsList({vehicleModels}) {
  return(
    <>
    <h1>Vehicle Models</h1>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
        </tr>
        </thead>
        <tbody>
        {vehicleModels.map((vehicleModel) => {
            return (
            <tr key={ vehicleModel.id }>
                <td className="align-middle">{ vehicleModel.name }</td>
                <td className="align-middle">{ vehicleModel.manufacturer.name }</td>
                <td className="align-middle"><img src={vehicleModel.picture_url} alt="" height="100" width="200"></img></td>
            </tr>
            );
        })}
        </tbody>
        </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink to="/vehicle_models/new" className="btn btn-primary btn-lg px-4 gap-3">Create a vehicle model</NavLink>
        </div>
    </>
  );
}

export default VehicleModelsList;
