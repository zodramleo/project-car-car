import React from 'react';
import { NavLink } from 'react-router-dom';


function TechniciansList({technicians}) {
  return(
    <>
    <h1>Technicians</h1>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Employee Number</th>
        </tr>
        </thead>
        <tbody>
        {technicians.map((technician) => {
            return (
            <tr key={ technician.id }>
                <td className="align-middle">{ technician.name }</td>
                <td className="align-middle">{ technician.employee_number }</td>
            </tr>
            );
        })}
        </tbody>
        </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink to="/technicians/new" className="btn btn-primary btn-lg px-4 gap-3">Add a technician</NavLink>
        </div>
    </>
  );
}

export default TechniciansList;
