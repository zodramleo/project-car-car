import React, {useState} from 'react';
import { useNavigate } from 'react-router-dom';


function SaleRecordForm({getSaleRecordData, salesperson, customer, autos, getAutosData}) {
  const [automobile, setAutomobile] = useState('')
  const [salesPerson, setSalesPerson] = useState('')
  const [customerId, setCustomerId] = useState('')
  const [salePrice, setPrice] = useState('')
  const handleAutomobileChange = (event) => {
      const value = event.target.value;
      setAutomobile(value);
  }
  const handleSalesPersonChange = (event) => {
      const value = event.target.value;
      setSalesPerson(value);
  }

  const handleCustomerChange = (event) => {
      const value = event.target.value;
      setCustomerId(value);
  }
  const handleSalePriceChange = (event) => {
      const value = event.target.value;
      setPrice(value);
  }
  const navigate = useNavigate();
  const handleSubmit = async (event) => {
      event.preventDefault()
      const data = {}
      data.automobile = automobile
      data.sales_person = salesPerson
      data.customer = customerId
      data.sale_price = parseFloat(salePrice)
      const salesUrl = "http://localhost:8090/api/salerecord/"
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          }
      }
      const response = await fetch(salesUrl, fetchConfig)
      if (response.ok) {
        getSaleRecordData();
        getAutosData()
        navigate('/salerecord');
      }
  }
  return(
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create Sale Record</h1>
            <form onSubmit={handleSubmit} id="create-salerecord-form">
            <div className="mb-3">
                <select onChange={handleAutomobileChange} value={automobile} required id="automobile" name="automobile" className="form-select">
                  <option value="">Choose an automobile</option>
                  {autos.filter(auto => auto.is_sold===false).map((filteredAuto) => {
                          return (
                            <option value={filteredAuto.vin} key={filteredAuto.id}>
                              {filteredAuto.vin}
                            </option>
                          );
                        })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={handleSalesPersonChange} value={salesPerson} required id="sales_person" name="sales_person" className="form-select">
                  <option value="">Choose a sales person</option>
                  {salesperson.map((sales) => {
                          return (
                            <option value={sales.id} key={sales.id}>
                              {sales.name}
                            </option>
                          );
                        })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={handleCustomerChange} value={customerId} required id="customerId" name="customerId" className="form-select">
                  <option value="">Choose a customer</option>
                  {customer.map((eachCustomer) => {
                          return (
                            <option value={eachCustomer.id} key={eachCustomer.id}>
                              {eachCustomer.name}
                            </option>
                          );
                        })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleSalePriceChange} value={salePrice} placeholder="sale_price" required type="number" name="salePrice" id="salePrice" className="form-control align-middle" />
                <label htmlFor="salePrice">Sale Price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  );
}

export default SaleRecordForm;
