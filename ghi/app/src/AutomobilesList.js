import React from 'react';
import { NavLink } from 'react-router-dom';


function AutomobilesList({automobiles}) {
  return(
    <>
    <h1>Vehicle Models</h1>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
        </tr>
        </thead>
        <tbody>
        {automobiles.map((automobile) => {
            return (
            <tr key={ automobile.id }>
                <td className="align-middle">{ automobile.vin }</td>
                <td className="align-middle">{ automobile.color }</td>
                <td className="align-middle">{ automobile.year }</td>
                <td className="align-middle">{ automobile.model.name }</td>
                <td className="align-middle">{ automobile.model.manufacturer.name }</td>
            </tr>
            );
        })}
        </tbody>
        </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink to="/automobiles/new" className="btn btn-primary btn-lg px-4 gap-3">Add an automobile to inventory</NavLink>
        </div>
    </>
  );
}

export default AutomobilesList;
