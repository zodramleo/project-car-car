import React from 'react';
import { NavLink } from 'react-router-dom';


function SalesPersonList({salesperson}) {
  return(
    <>
    <h1>Sales Person</h1>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Employee Number</th>
        </tr>
        </thead>
        <tbody>
        {salesperson.map((sales_person) => {
            return (
            <tr key={ sales_person.id }>
                <td className="align-middle">{ sales_person.name }</td>
                <td className="align-middle">{ sales_person.employee_number }</td>
            </tr>
            );
        })}
        </tbody>
        </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink to="/salesperson/new" className="btn btn-primary btn-lg px-4 gap-3">Add a sales person</NavLink>
        </div>
    </>
  );
}

export default SalesPersonList;
