import React, { useState } from 'react';


function CustomerForm({getCustomerData, customer}) {
    const [name, setName] = useState('')
    const [address, setAddress] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }
    const handlePhoneChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.address = address;
        data.phone_number = phoneNumber;
        const url = "http://localhost:8090/api/customer/";
            const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
            };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
        setName('');
        setAddress('');
        setPhoneNumber('');
        getCustomerData()
        }
    }
    return(
        <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Enter a customer</h1>
                <form onSubmit={handleSubmit} id="create-customer-form">
                  <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleAddressChange} value={address} placeholder="address" required type="text" name="address" id="address" className="form-control align-middle" />
                    <label htmlFor="address">Address</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handlePhoneChange} value={phoneNumber} placeholder="phoneNumber" required type="text" name="phoneNumber" id="phoneNumber" className="form-control align-middle" />
                    <label htmlFor="address">Phone Number</label>
                  </div>
                  <button className="btn btn-primary">Add</button>
                </form>
                <h2>Customers</h2>
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                    </tr>
                    </thead>
                    <tbody>
                    {customer.map((customer) => {
                        return (
                        <tr key={ customer.id }>
                            <td className="align-middle">{ customer.name }</td>
                            <td className="align-middle">{ customer.address }</td>
                            <td className="align-middle">{ customer.phone_number }</td>
                        </tr>
                        );
                    })}
                    </tbody>
                    </table>
                        </div>
                        </div>
                    </div>
      );
}

export default CustomerForm;
