from common.json import ModelEncoder

from .models import AutomobileVO, SalesPerson, SaleRecord, Customer


class VOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "is_sold",
        "import_href",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number"
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number"
    ]

class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "id",
        "sale_price",
        "customer",
        "sales_person",
        "automobile"
    ]
    encoders = {
        "automobile": VOEncoder(),
        "customer": CustomerEncoder(),
        "sales_person": SalesPersonEncoder(),

    }
