from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, SaleRecord, SalesPerson, Customer
from .encoders import VOEncoder, SaleRecordEncoder, SalesPersonEncoder, CustomerEncoder


@require_http_methods(["GET", "POST"])
def api_list_sales_person(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except Exception as ex:
            response = JsonResponse(
                {"message": "Could not create employee"},
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_sales_person(request, employee_number):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(employee_number=employee_number)
            return JsonResponse(
                sales_person,
                SalesPersonEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not find employee"}
            )
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sales_person = SalesPerson.objects.get(employee_number=employee_number)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not find employee"}
            )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(employee_number=employee_number)
            props = ["name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(sales_person, prop, content[prop])
            sales_person.save()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message": "Employee does not exist"},
                status=404,
                )
            return response


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder = CustomerEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder = CustomerEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                CustomerEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not find customer"}
            )
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not find customer"}
            )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=id)
            props = ["name", "address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist"},
                status=404,
            )
            return response


@require_http_methods(["GET", "POST"])
def api_list_sale_record(request):
    if request.method == "GET":
        sale_record = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_record": sale_record},
            encoder=SaleRecordEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            response = JsonResponse(
                {"message": "Car not found"},
            )
            response.status_code = 400
            return response
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer not found"},
            )
            response.status_code = 400
            return response
        try:
            sales_person = SalesPerson.objects.get(id=content["sales_person"])
            content["sales_person"] = sales_person
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message": "Sales Person not found"},
            )
            response.status_code = 400
            return response
        sale_record = SaleRecord.objects.create(**content)
        automobile.is_sold = True
        automobile.save()
        return JsonResponse(
            sale_record,
            encoder=SaleRecordEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def api_sale_record_details(request, id):
    if request.method == "GET":
        try:
            sale_record = SaleRecord.objects.get(id=id)
            return JsonResponse(
                sale_record,
                encoder=SaleRecordEncoder,
                safe=False,
            )
        except SaleRecord.DoesNotExist:
            response = JsonResponse(
                {"message": "Record not found"},
                status=404,
            )
            return response
    else:
        content = json.loads(request.body)
        try:
            content["sales_person"] = SalesPerson.objects.get(name=content["sales_person"])
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=404
            )
        try:
            content["customer"] = Customer.objects.get(name=content["customer"])
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404
            )

        SaleRecord.objects.filter(id=id).update(**content)
        sales_record = SaleRecord.objects.get(id=id)
        return JsonResponse(
            sales_record,
            encoder=SaleRecordEncoder,
            safe=False
        )


@require_http_methods(["GET"])
def api_automobiles(request):
    if request.method == "GET":
        automobile = AutomobileVO.objects.all()
        return JsonResponse(
            {"autos": automobile},
            encoder=VOEncoder,
            safe=False,
        )
