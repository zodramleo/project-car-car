from django.urls import path

from .views import api_list_sales_person, api_show_sales_person, api_list_customer
from .views import api_show_customer, api_list_sale_record, api_sale_record_details, api_automobiles


urlpatterns = [
    path(
        "salesperson/",
        api_list_sales_person,
        name="api_list_sales_person",
    ),
    path(
        "salesperson/<int:employee_number>/",
        api_show_sales_person,
        name="api_show_sales_person",
    ),
    path(
        "customer/",
        api_list_customer,
        name="api_list_customer",
    ),
    path(
        "customer/<int:id>",
        api_show_customer,
        name="api_show_customer",
    ),
    path(
        "salerecord/",
        api_list_sale_record,
        name="api_list_sale_record",
    ),
    path(
        "salerecord/<int:employee_number>",
        api_sale_record_details,
        name="api_sale_record_details",
    ),
    path(
        "automobiles/",
        api_automobiles,
        name="api_automobiles"
    )

]
